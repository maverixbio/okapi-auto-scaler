var request = require('request');
var async = require('async');
require( "console-stamp" )( console);
var options = [
  {
    name : "cluster",
    type : "string"
  },
  {
    name : "containerAllocatedDefault",
    type : "string"
  }
]
var args = require('argv').option(options).run();
var markNodesForRelease = {};

var clusterName = args.options.cluster;
var noTasksToExecute = 0;
var containerAllocatedDefault = args.options.containerAllocatedDefault || 1;
console.log(clusterName);

function checkClusterState(cb) {
  console.log("Checking cluster state");
  function check(activeNodes, pendingTasks, containersAllocated, cb) {
    if(activeNodes > 1 || pendingTasks == 0) {
      if(activeNodes > 1 && containersAllocated == containerAllocatedDefault && pendingTasks == 0)
        noTasksToExecute++;
      if(noTasksToExecute > 3) {
        noTasksToExecute = 0;
        return releaseActiveNodes(function(){
           return cb("Nodes released done");
        });
      }
      return cb("Nothing to be done");
    }
    noTasksToExecute = 0;
    cb(null);
  }
  console.log("http://dev-mav-"+clusterName + ".useast1.maverixbio.com:8088/ws/v1/cluster/metrics");
  request.get("http://dev-mav-"+clusterName + ".useast1.maverixbio.com:8088/ws/v1/cluster/metrics", function(err, res, body){
    console.log(err);
    console.log(body);
    var data = JSON.parse(body);
    var activeNodes = data.clusterMetrics.activeNodes;
    var pendingTasks = data.clusterMetrics.containersPending;
    var containersAllocated = data.clusterMetrics.containersAllocated;
    console.log("Active nodes : " + activeNodes);
    console.log("Containers pending : " + pendingTasks);
    console.log("Containers Allocated : " + containersAllocated);
    return check(activeNodes, pendingTasks, containersAllocated, cb);
  });
}

var reservedNodeNamesList = ['01', '10', '11', 'eone11', "101"];

function releaseActiveNodes(cb) {
  request.get("http://dev-mav-"+clusterName + ".useast1.maverixbio.com:8088/ws/v1/cluster/nodes", function(err, res, body){
    var data = JSON.parse(body);
    var nodesToRelease = [];
    data.nodes.node.forEach(function(node) {
      var parts = node.nodeHostName.split(".")[0].split("-");
      if(node.state == 'RUNNING' && reservedNodeNamesList.indexOf(parts[parts.length - 1]) == -1) {
        nodesToRelease.push(node.nodeHostName);
      }
    });
    if(nodesToRelease.length == 0) {
      console.log("No nodes required to be released");
      return cb();
    }
    console.log("Nodes to release - " + nodesToRelease);
    var exec = require('child_process').exec;
    exec('./releaseNodes.sh ' + clusterName + ' ' + nodesToRelease.join(" "), function(err, stdout, stderr){
      console.log(stdout);
      console.log(stderr);
      var nodeNamesToDelete = nodesToRelease.map(function(name){
        var parts = name.split(".");
        return parts[0] + "." + parts[1];
      });
      exec("./deleteInst.sh " + nodeNamesToDelete.join(" "), function(err, stdout, stderr){
      console.log(stdout);
      console.log(stderr);
        console.log("Nodes deleted are " + nodeNamesToDelete);
        return cb();
      });
    });
  });
}

var tags = [];
function spinOfNode(cb) {
  console.log("Spinning of new node");
  var exec = require('child_process').exec;
  exec(
      '/opt/ops-awstools/bin/newInstance -r us-east-1a -o hvm-aws2014032 -f okapi -e dev -d maverixbio.com -s r3.2xlarge -O mav -S dyn -n public -v vpc-fd138b96 -p spot -P 0.30 -u lsubramanian',
      function(err, stdout, stderr) {
        var data = stdout.toString().split("\n");
        data.forEach(function(line) {
        if(/^TAG/.test(line)) {
          var parts = line.split("\t");
          if(parts[1] == 'instance') {
            var tag = parts[2].split("-");
            if(tags.indexOf(tag[1]) == -1) {
              tags.push(tag[1]);
            }
          }
        }
        });
      });
   return cb();
}

var scriptForCluster = {
  "okapi-01" : "./cluster_test.sh ",
  "okapi-10" : "./cluster.sh",
  "okapi-eone11" : "./cluster_eone.sh",
  "okapi-101" : "./cluster_101.sh"
};

function addToCluster(cb) {
  setTimeout(function() {
    console.log("Adding instances to the cluster - " + tags);
    var exec = require('child_process').exec;
    exec(scriptForCluster[clusterName] + " " + tags.join(" "), function(err, stdout, stderr) {
      console.log(stdout);
      console.log("Err : " + stderr);
      cb();
    });
  }, 1000* 60 * 30 );
}

function keepChecking() {
  tags = [];
  console.log("Start to check");
  async.series([checkClusterState, spinOfNode, spinOfNode, spinOfNode, spinOfNode, addToCluster], function(err) {
  //async.series([addToCluster], function(err) {
    var timeout = 1000 * 60 * 5;
    if(err) {
      console.log(err);
    }
    console.log("Done");
    setTimeout(function() {
      keepChecking();
    }, timeout);
  });
}
keepChecking();
/*tags = ["9d8d9d1d"];
addToCluster(function() {
  console.log("done");
});*/